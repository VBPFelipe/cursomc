package com.felipe.cursomc.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.felipe.cursomc.domain.Cliente;
import com.felipe.cursomc.services.ClienteService;

/**
 * 
 * @author Felip
 *
 */
@RestController
@RequestMapping(value="/clientes")
public class ClienteResource {
	
	@Autowired
	private ClienteService service;
	
	@RequestMapping( value="/{id}", method=RequestMethod.GET)
//	@GetMapping
	public ResponseEntity<?> find(@PathVariable Integer id) {
		
		/* 
		 * Controladores REST costuma ter métodos pequenos
		 * Tratamento try-catch pode alongar muito o código
		 *  Alternativa: Handler!
		try {
		Cliente obj = service.find(id);
		return ResponseEntity.ok().body(obj);
		}catch() {
		}
		 */
		Cliente obj = service.find(id);
		return ResponseEntity.ok().body(obj);
		
//		Cliente cli1 = new Cliente(null, "Maria Silva", "maria@gmail.com", "36378912377", TipoCliente.PESSOAFISICA);
//		Cliente cliente2 = new Cliente( , );
//		
//		/*
//		 * List é uma interface, e não pode ser instanciado.
//		 * Portanto, é preciso de uma classe que implemente essa interface,
//		 *  para então instanciar a interface List.
//		 */
//		List<Cliente> clientes = new ArrayList<>();
//		clientes.add(cliente1);
//		clientes.add(cliente2);
//		
//		return clientes;
	}
}
